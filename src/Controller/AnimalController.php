<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Repository\AnimalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class AnimalController extends AbstractController
{

    private $animalRepository;

    public function __construct(AnimalRepository $animalRepository)
    {
        $this->animalRepository = $animalRepository;
    }

    /**
     * Display crawled animals in raw html lines
     *
     * @Route("animals/crawling/raw")
     *
     * @return Response
     */
    public function rawCrawling(): Response
    {
        $animals
            = $this->animalRepository->getAnimalsFromLeBonCoinCrawling(100);
        $i = 0;

        return new Response(implode('',
            $animals->map(function (Animal $animal) use (&$i) {
                $i++;

                return sprintf("%s. %s %s",
                    sprintf('%03d', $i),
                    $animal->toString(),
                    "<br/>");
            })->toArray()));
    }

    /**
     * Save crawled animals in database
     *
     * @Route("animals/crawling/save")
     *
     * @return Response
     */
    public function saveCrawledAnimals(): Response
    {
        if ($this->animalRepository->saveAnimalsFromLeBonCoinCrawling(100)) {
            return new Response('Crawled animals imported.');
        }

        return new Response('Crawled animals could not be imported.');
    }

    /**
     * @Route("/animals/{id}/click", methods={"POST"})
     * @param int              $id
     * @param AnimalRepository $animalRepository
     *
     * @return Response
     */
    public function incrementClicks(int $id, AnimalRepository $animalRepository): Response
    {
        if ($this->has('session')) {
            $session = $this->get('session');
        } else {
            $session = new Session();
            $session->start();
        }

        $session = $this->get('session');

        if (!$session->get('id')) {
            $animal = $animalRepository->find($id);
            $animal->incrementClicks();
            $this->getDoctrine()->getManager()->flush();
            $session->set($id, true);
        }
        return new Response();
    }

}

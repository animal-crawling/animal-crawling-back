<?php

namespace App\Repository;

use App\Collection\AnimalCollection;
use App\Entity\Animal;
use App\Service\LeBonCoinCrawlingService;
use App\Transformer\AnimalCollectionTransformer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Animal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Animal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Animal[]    findAll()
 * @method Animal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimalRepository extends ServiceEntityRepository
{
    private $leBonCoinCrawlingService;

    public function __construct(
        ManagerRegistry $registry,
        LeBonCoinCrawlingService $leBonCoinCrawlingService
    ) {
        parent::__construct($registry, Animal::class);
        $this->leBonCoinCrawlingService = $leBonCoinCrawlingService;
    }

    /**
     * Get Animals from LeBonCoin Ads crawling
     *
     * @param $limit
     *
     * @return AnimalCollection
     */
    public function getAnimalsFromLeBonCoinCrawling($limit): AnimalCollection
    {
        return AnimalCollectionTransformer::createFromAdCollection($this->leBonCoinCrawlingService->fetchAds($limit));
    }

    /**
     * Save Animals from LeBonCoin Ads Crawling
     *
     * @param $limit
     *
     * @return bool
     */
    public function saveAnimalsFromLeBonCoinCrawling($limit): bool
    {
        $animals = $this->getAnimalsFromLeBonCoinCrawling($limit);
        $entityManager = $this->getEntityManager();

        $animals->forAll(function ($k, Animal $animal) use ($entityManager) {
            $entityManager->persist($animal);

            return true;
        });

        try {
            $entityManager->flush();
            return true;
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }

        return false;
    }
}

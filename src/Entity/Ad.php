<?php

namespace App\Entity;

class Ad
{
    private $title;
    private $location;
    private $price;
    private $externalId;

    /**
     * Ad constructor.
     *
     * @param string $title
     * @param string $location
     * @param float  $price
     * @param string $externalId
     */
    public function __construct(
        string $title,
        string $location,
        ?float $price,
        string $externalId
    ) {

        $this->title = $title;
        $this->location = $location;
        $this->price = $price;
        $this->externalId = $externalId;
    }

    public function toString(): string
    {
        return sprintf("%s - %s - %s", $this->title, $this->location,
            $this->price
                ? sprintf("%s€", number_format($this->price, 2, '.', ''))
                : 'N/A');
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return null|float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

}

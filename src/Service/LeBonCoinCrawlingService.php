<?php

namespace App\Service;

use App\Collection\AdCollection;
use App\Exception\CrawlingException;
use App\Transformer\AdCollectionTransformer;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Throwable;

class LeBonCoinCrawlingService
{
    private const BASE_URL = "https://www.leboncoin.fr/animaux/offres/auvergne_rhone_alpes/";
    private const ADS_LIST_SELECTOR = 'ul > li[data-qa-id="aditem_container"]';
    public const TITLE_SELECTOR = 'span[data-qa-id="aditem_title"]';
    public const LOCATION_SELECTOR = 'p[data-qa-id="aditem_location"]';
    public const PRICE_SELECTOR = 'div[data-qa-id="aditem_price"] > span > span[itemprop="priceCurrency"]';
    public const EXTERNAL_ID_SELECTOR = 'a[href^="/animaux/"][href$=".htm/"]';

    private $publicDir;

    private $client;

    public function __construct(
        HttpClientInterface $client,
        string $publicDir
    ) {
        $this->client = $client;
        $this->publicDir = $publicDir;
    }

    /**
     * Fetch LeBonCoin Web Page data
     *
     * @param $page
     *
     * @return bool|ResponseInterface
     * @throws TransportExceptionInterface
     * @throws CrawlingException
     */
    private function fetchLeBonCoinWebPage($page)
    {
        $response = $this->client->request('GET',
            sprintf("%s/p-%s/", self::BASE_URL, $page));
        if ($response->getStatusCode() === Response::HTTP_OK) {
            return $response;
        }
        throw new CrawlingException('Could not fetch LeBonCoin.');
    }

    /**
     * Get HTML as a string from LeBonCoin local page
     *
     * @param $page
     *
     * @return false|string
     * @throws CrawlingException
     */
    private function fetchLeBonCoinLocalPage($page)
    {
        $filePath = "$this->publicDir/leboncoin/animaux-p{$page}.html";
        if (!file_exists($filePath)) {
            throw new CrawlingException('Could not local page LeBonCoin.');
        }

        return file_get_contents($filePath);
    }

    /**
     * Get LeBonCoin ads from an url
     *
     * @param $limit
     *
     * @return AdCollection
     */
    public function fetchAds($limit): AdCollection
    {
        $ads = new AdCollection();
        $page = 1;
        $leBonCoinWebPage = "";
        while ($ads->count() <= $limit) {
            try {
                // Fetching ads from LeBonCoin remote website
                $leBonCoinWebPage = $this->fetchLeBonCoinWebPage($page);
            } catch (Throwable $e) { // LeBonCoin Forbidden Access
                // Fallback on local files
                try {
                    $leBonCoinWebPage = $this->fetchLeBonCoinLocalPage($page);
                } catch (CrawlingException $e) {
                }
            }

            $newCrawledAds = new Crawler($leBonCoinWebPage);
            $ads
                = $ads->merge(AdCollectionTransformer::createFromCrawler($newCrawledAds->filter(self::ADS_LIST_SELECTOR)));
            $page++;
        }

        return $ads;
    }
}

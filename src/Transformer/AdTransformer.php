<?php

namespace App\Transformer;

use App\Entity\Ad;
use App\Service\LeBonCoinCrawlingService;
use Symfony\Component\DomCrawler\Crawler;

class AdTransformer
{
    /**
     * Create an Ad Entity from crawled ad
     *
     * @param Crawler $crawler
     *
     * @return Ad
     */
    public static function createFromCrawler(Crawler $crawler): Ad
    {
        $title = $crawler->filter(LeBonCoinCrawlingService::TITLE_SELECTOR);
        $location
            = $crawler->filter(LeBonCoinCrawlingService::LOCATION_SELECTOR);
        $price = $crawler->filter(LeBonCoinCrawlingService::PRICE_SELECTOR);
        $externalId
            = $crawler->filter(LeBonCoinCrawlingService::EXTERNAL_ID_SELECTOR);

        return new Ad(
            (bool)$title->count() ? $title->text() : null,
            (bool)$location->count() ? $location->text() : null,
            ((bool)$price->count()
                ? ((float)str_replace('€', '', $price->text()))
                : null),
            (bool)$externalId->count() ?
                (preg_match('/\/animaux\/(.*?)\.htm\//',
                    $externalId->attr('href'), $match) === 1
                    ? $match[1]
                    : null)
                : null
        );
    }

}

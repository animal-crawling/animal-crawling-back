<?php

namespace App\Transformer;

use App\Entity\Ad;
use App\Entity\Animal;

class AnimalTransformer
{
    /**
     * Create an Animal entity from Ad entity
     *
     * @param $ad
     *
     * @return Animal
     */
    public static function createFromAd(Ad $ad): Animal
    {
        $animal = new Animal();
        $animal->setTitle($ad->getTitle());
        $animal->setLocation($ad->getLocation());
        $animal->setPrice($ad->getPrice());
        $animal->setExternalId($ad->getExternalId());
        $animal->initClicks();
        return $animal;
    }
}

<?php

namespace App\Transformer;

use App\Collection\AdCollection;
use App\Collection\AnimalCollection;
use App\Entity\Ad;

class AnimalCollectionTransformer
{
    public static function createFromAdCollection(AdCollection $adCollection): AnimalCollection {
        return new AnimalCollection($adCollection->map(function(Ad $ad){
            return AnimalTransformer::createFromAd($ad);
        })->toArray());
    }
}

<?php

namespace App\Transformer;

use App\Collection\AdCollection;
use Symfony\Component\DomCrawler\Crawler;

class AdCollectionTransformer
{
    /**
     * Create an AdCollection from crawled ads
     *
     * @param Crawler $crawler
     *
     * @return AdCollection
     */
    public static function createFromCrawler(Crawler $crawler): AdCollection
    {
        $adCollection = new AdCollection();

        $crawler->each(function (Crawler $adCrawler) use ($adCollection) {
            $adCollection->add(AdTransformer::createFromCrawler($adCrawler));
        });

        return $adCollection;
    }
}

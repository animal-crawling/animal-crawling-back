<?php

namespace App\Collection;

use Doctrine\Common\Collections\ArrayCollection;

class AdCollection extends ArrayCollection
{
    public function merge(AdCollection $adCollectionToMerge): ArrayCollection
    {
        return new self(
            array_merge($this->toArray(), $adCollectionToMerge->toArray())
        );
    }
}

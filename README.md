# Animal Crawling - Back (Symfony 4.4)
![alt text](./public/logo-small-w-padding.png "Animal Crawling Logo")

### Getting started
```
> composer install
> php bin/console doctrine:database:create
> php bin/console doctrine:migrations:migrate
> symfony server:start
```

### Tech. featured
* [x] **PHP 7.4.11**
* [x] **Symfony 4.4.15 (LTS)**
* [x] **SQLite**
* [x] **API Platform**

<br/>

###### 2) Récupérer dynamiquement la liste des 100 premiers résultats de la catégorie Animaux en RhôneAlpes sur leboncoin.fr, avec les informations suivantes : titre, lieu, prix
- Route : */animals/crawling/raw*

###### 3) Stocker cette liste dans une base de données et les mapper via un ORM. 
- Route : */animals/crawling/save*

###### 3) Depuis cette base de données 
- a. Afficher cette liste par ordre de prix, avec ceux sans tarif spécifié à la fin <br/>
Route : */api/animals?order[price]=desc*

- b. Permet une recherche par titre <br/>
Route : */api/animals?title=chien*

- c. Permettre de filtrer par prix minimum & maximum  <br/>
Route : */api/animals?price[between]=5.5..66.6*
